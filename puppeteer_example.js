const puppeteer = require('puppeteer');

(async () => {
  // Launch a new browser instance
  const browser = await puppeteer.launch({
		headless: false,
		slowMo: 250,
		userDataDir: '/Users/iagh/Library/Application Support/Google/Chrome/Profile 1'
	});

  // Create a new page
  const page = await browser.newPage();

  // Navigate to the Google sign-in page
  await page.goto('https://accounts.google.com');

  // Check if the user is already signed in
  const signedIn = await page.evaluate(() => {
  // Modify this evaluation logic based on the elements or conditions that indicate the user is signed in
  const signOutButton = document.querySelector('#gb');
  return !!signOutButton; // Return true if signOutButton element is found, indicating the user is signed in
  });

  if (!signedIn) {
		const dataIdentifierElement = await page.$('[data-identifier="michal@slant.cz"]');
		if (dataIdentifierElement) {
			// Click the data-identifier element to continue with login
			await dataIdentifierElement.click();
		} else {
			// Fill in the email field and click "Next"
			await page.type('input[type="email"]', 'michal@slant.cz');
			await page.click('#identifierNext');
		}

	  // Wait for the password field to be visible
	  await page.waitForSelector('input[type="password"]');

	  // Fill in the password field and click "Next"
	  await page.type('input[type="password"]', 'SEEN!whuy0bult');
	  await page.click('#passwordNext');

	  // Wait for the user to be signed in
	  await page.waitForNavigation();
  }

  // Perform web automation actions
  // e.g., click buttons, fill forms, extract data

  // Close the browser
  await browser.close();
})();
