const puppeteer = require('puppeteer');
const fs = require('fs');

// Function to wait for an element with a given selector to be present on the page
async function waitForSelector(page, selector, options = {}) {
	try {
		await page.waitForSelector(selector, options);
	} catch (error) {
		console.log(`Error: Element with selector "${selector}" not found.`);
		throw error;
	}
}

async function run(page, folderItem) {
	// Extract the folder name
	const folderName = await folderItem.$eval('button[aria-label]', (button) =>
		button.getAttribute('aria-label')
	);
	console.log(`Processing folder: ${folderName}`);

	async function scrollIntoViewByScrollbar(item) {
		await page.evaluate((element) => {
		return new Promise((resolve) => {
			const scrollParent = document.querySelector('[data-automation-id="scrollbar"]');
			// Calculate the element's position relative to the scroll parent
			const elementRect = element.getBoundingClientRect();
			const parentRect = scrollParent.getBoundingClientRect();
			const offsetTop = elementRect.top - parentRect.top;
			const offsetBottom = elementRect.bottom - parentRect.bottom;

			// Scroll to bring the element into view if needed
			if (offsetTop < 0 || offsetBottom > 0) {
				scrollParent.scrollTop += offsetTop; // You can adjust this value as needed
				// Resolve the promise when scrolling is complete
				scrollParent.addEventListener('scroll', () => {
				resolve();
				}, { once: true });
			} else {
				// If no scrolling needed, resolve immediately
				resolve();
			}
		});
		}, item);
	}

	const lazyGrid = await page.$('[data-automation-id="LazyGrid"]');
	const hrefs = new Set();
	await page.waitForTimeout(1000);
	const footerElement = await page.waitForSelector('div.wf-ila07m > footer > span');
	const itemCountText = await footerElement.evaluate((el) => el.innerText);
	const itemCount = parseInt(itemCountText, 10);
	console.log('ITEM COUNT: ', itemCount);

	while (hrefs.size < itemCount) {
		await page.waitForTimeout(1000); // Wait for lazy loaded assets to appear
		const assetGridItems = await lazyGrid.$$('.bem-AssetGrid_Item');

		if (assetGridItems.length === 0) {
			console.log('No more items found. Reached the end.');
			break;
		}

		await scrollIntoViewByScrollbar(assetGridItems[0]);

		for (const assetGridItem of assetGridItems) {
			console.log('SIZE: ', hrefs.size, assetGridItems.length, itemCount);

			const isFullyVisible = await page.evaluate((parent, child) => {
				const parentRect = parent.getBoundingClientRect();
				const childRect = child.getBoundingClientRect();

				return (
					childRect.top >= parentRect.top &&
					childRect.bottom <= parentRect.bottom &&
					childRect.left >= parentRect.left &&
					childRect.right <= parentRect.right
				);
			}, lazyGrid, assetGridItem);

			if (!isFullyVisible) {
				await assetGridItem.scrollIntoViewIfNeeded();
			}

			// const domElement = await assetGridItem.evaluate((element) => {
			// 	return element.title; // The DOM element represented by the ElementHandle
			// });
			// console.log('IN VIEW: ', domElement, isFullyVisible);

			await assetGridItem.hover(); // Simulate mouse enter

			// Wait for asset details button
			await waitForSelector(page, '[data-automation-id="asset-details-button"]');

			// Click on asset details button
			await page.click('[data-automation-id="asset-details-button"]');
			// console.log('CLICK');

			// Wait for asset details popover
			await waitForSelector(page, '[data-automation-id="asset-details-popover"]');

			const href = await page.$eval('[data-sc="link"]', (element) => element.href);
			console.log('href', hrefs.size + 1, folderName, href);

			const csvObj = [folderName + hrefs.size, folderName + hrefs.size, href, folderName];
			const arrayExists = Array.from(hrefs).some(
			(arr) => JSON.stringify(arr) === JSON.stringify(csvObj)
			);
			if (!arrayExists) {
				hrefs.add(csvObj);
			}

			// Close asset details popover
			await page.click('[data-sc="CloseButton"]');
			await waitForSelector(page, '[data-automation-id="asset-details-popover"]', { hidden: true });
		}

		await page.evaluate(() => {
			const scrollElement = document.querySelector('[data-automation-id="scrollbar"]');
			const height = scrollElement.offsetHeight;
			const top = scrollElement.scrollTop;
			const scroll = scrollElement.scrollHeight;
			if (top + height < scroll) {
				document.querySelector('[data-automation-id="scrollbar"]').scrollBy({ top: height });
			} else {
				document.querySelector('[data-automation-id="scrollbar"]').scrollTo({ top: scroll - height });
			}
		});

		const scrollbarElement = await page.waitForSelector('[data-automation-id="scrollbar"]');
		let currentScroll = await page.evaluate((scrollbar) => scrollbar.scrollTop, scrollbarElement);

		console.log('---------SCROLL---------- ', currentScroll);
	}

	return Array.from(hrefs);
}

(async () => {
	const browser = await puppeteer.launch({
		headless: 'new',
		// slowMo: 200,
		// MACOS
		// executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
		// userDataDir: '/Users/iagh/Library/Application Support/Google/Chrome/Profile 1'
		// UBUNTU
		executablePath: '/usr/bin/google-chrome-stable',
		userDataDir: '/home/iagh/.config/google-chrome/Profile 3', // need to copy folder of profile to ./Profile 3/Default
	});

	const page = await browser.newPage();
	await page.setViewport({ width: 1200, height: 800 });

	await page.goto('https://webflow.com/design/jatvar-cz');

	await page.waitForFunction(() => {
		const element = document.querySelector('.workspace.media-main');
		return element && element.getAttribute('data-cy') === 'loaded';
	 });

	await page.waitForSelector('[data-automation-id="left-sidebar-assets-button"]');
	console.log('PAGE LOADED');
	await page.click('[data-automation-id="left-sidebar-assets-button"]');
	console.log('ASSETS OPENED');

	await page.waitForSelector('[data-automation-id="Expand to Full View"]');
	await page.click('[data-automation-id="Expand to Full View"]');
	console.log('FULL VIEW OPENED');

	await page.waitForSelector('[data-automation-id="FolderList"] > div:nth-child(1) > div');
	await page.click('[data-automation-id="FolderList"] > div:nth-child(1) > div');
	console.log('FOLDER LIST EXPANDED');

	// Wait for FolderItem elements to be visible on the page
	await page.waitForSelector('[data-automation-id="FolderList"] > div >[data-automation-id="FolderItem"]');

	const allHrefs = [];

	const folderItems = await page.$$('[data-automation-id="FolderItem"]');
	const container = await page.$('[data-automation-id="AssetsTabFullView"] > div:nth-child(4) > div:nth-child(1)');

	for (const folderItem of folderItems) {
		// Scroll to the folder item by modifying the transform property
		await page.evaluate((ct, fi) => {
			const containerRect = ct.getBoundingClientRect();
			const folderItemRect = fi.getBoundingClientRect();
			const containerScrollTop = ct.scrollTop;

			// Calculate the translation value for scrolling
			const translationValue = folderItemRect.top - containerRect.top + containerScrollTop;

			// Set the transform property to scroll
			ct.style.transform = `translateY(-${translationValue}px)`;
		}, container, folderItem);

		await folderItem.click(); // Click on the folder item to open LazyGrid

		// Wait for LazyGrid to load or check if it exists
		await waitForSelector(page, '[data-automation-id="LazyGrid"]', { timeout: 5000 }).catch(() => {
			console.log('LazyGrid not found. Continuing to the next folder item.');
			return; // Continue to the next folder item
		});

		const hrefs = await run(page, folderItem);
		allHrefs.push(...hrefs);
	}

	const csvContent = `Name,Popis,Fotka,Kategorie\n${allHrefs.map((row) => row.join(',')).join('\n')}`;
	fs.writeFileSync('hrefs.csv', csvContent);

	console.log('END');
	await browser.close();
})();
