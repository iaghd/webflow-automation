require('dotenv').config();
const axios = require('axios');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// Replace 'your_site_id' with your actual Webflow site ID
const siteId = '643566cf5efcf246a0d56dad';
const assetFoldersUrl = `https://api.webflow.com/v2/sites/${siteId}/asset_folders`;
const assetsUrl = `https://api.webflow.com/v2/sites/${siteId}/assets`;

const csvWriter = createCsvWriter({
  path: 'webflow-assets.csv',
  header: [
    {id: 'name', title: 'Name'},
    {id: 'fotka', title: 'Fotka'},
    {id: 'kategorie', title: 'Kategorie'}
  ]
});

// Setup your API token in a .env file as WEBFLOW_API_TOKEN=your_token_here
const config = {
  headers: {
    accept: 'application/json',
    authorization: `Bearer ${process.env.WEBFLOW_API_TOKEN}`,
  }
};

const fetchAllPages = async (baseUrl) => {
  let results = [];
  let offset = 0;
  let total = 0;
  do {
    const response = await axios.get(`${baseUrl}?offset=${offset}`, config);
    if (response.data.pagination) {
      total = response.data.pagination.total;
      offset += response.data.pagination.limit;
    }
    results = results.concat(response.data.assetFolders || response.data.assets);
    console.log('URL:: ', offset, total);
  } while (offset < total);
  return results;
};

const fetchAssetFolders = async () => {
  try {
    const response = await axios.get(assetFoldersUrl, config);
    return response.data.assetFolders;
  } catch (error) {
    console.error('Error fetching asset folders:', error.message);
    return [];
  }
};

const fetchAssets = async () => {
  try {
    const response = await axios.get(assetsUrl, config);
    return response.data.assets;
  } catch (error) {
    console.error('Error fetching assets:', error.message);
    return [];
  }
};

const writeAssetsToCSV = async (assetFolders, assets) => {
  const records = [];

  assetFolders.forEach((folder) => {
    folder.assets.forEach((assetId, assetIndex) => {
      const asset = assets.find(asset => asset.id === assetId);
      if (asset) {
        records.push({
          name: `${folder.displayName}-${assetIndex + 1}`,
          fotka: `https://uploads-ssl.webflow.com/${siteId}/${asset.displayName}`,
          kategorie: folder.displayName
        });
      }
    });
  });

  await csvWriter.writeRecords(records)
    .then(() => console.log('The CSV file was written successfully'));
};

const run = async () => {
  const assetFolders = await fetchAllPages(assetFoldersUrl);
  const assets = await fetchAllPages(assetsUrl);

  await writeAssetsToCSV(assetFolders, assets);
};

run();
